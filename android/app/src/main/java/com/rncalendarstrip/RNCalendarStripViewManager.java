package com.rncalendarstrip;

import android.support.annotation.Nullable;
import android.view.View;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.util.Map;

public class RNCalendarStripViewManager extends SimpleViewManager<RNCalendarStripView> {

    @Override
    public String getName() {
        return RNCalendarStripConstants.RN_CALENDAR_STRIP_TAG;
    }

    @Override
    protected RNCalendarStripView createViewInstance(ThemedReactContext reactContext) {
        return new RNCalendarStripView(reactContext.getBaseContext());
    }

    @ReactProp(name = RNCalendarStripConstants.DAY_NAME, defaultInt = 12)
    public void setRNCalendarStripDayNameSize(RNCalendarStripView view, int size) {
        view.setDayNameSize(size);
    }

    @ReactProp(name = RNCalendarStripConstants.DAY_NUMBER, defaultInt = 17)
    public void setRNCalendarStripDayNumberSize(RNCalendarStripView view, int size) {
        view.setDayNumberSize(size);
    }

    @ReactProp(name = RNCalendarStripConstants.HEIGHT, defaultInt = 100)
    public void setRNCalendarStripListHeight(RNCalendarStripView view, int height) {
        view.setListHeight(height);
    }

    @ReactProp(name = RNCalendarStripConstants.WIDTH, defaultInt = 100)
    public void setRNCalendarStripListWidth(RNCalendarStripView view, int width) {
        view.setListWidth(width);
    }

    @ReactProp(name = RNCalendarStripConstants.MARGIN, defaultInt = 10)
    public void setRNCalendarStripMargin(RNCalendarStripView view, int value) {
        view.setMargin(value);
    }

    @ReactProp(name = RNCalendarStripConstants.ROW_WIDTH, defaultInt = 200)
    public void setRNCalendarStripRowsWidth(RNCalendarStripView view, int width) {
        view.setRowWidth(width);
    }

    @ReactProp(name = RNCalendarStripConstants.DATE_FORMAT)
    public void setRNStripCalendarDateFormat(RNCalendarStripView view, @Nullable String format) {
        view.setDateFormat(format);
    }

    @ReactProp(name = RNCalendarStripConstants.DATE_PROPERTY_NAME)
    public void setRNStripCalendarDatePropeertyName(RNCalendarStripView view, @Nullable String name){
        view.setDatePropertyName(name);
    }

    /**
     * map all date events tags to call from js code
     * @return map date of events names
     */
    @Nullable
    @Override
    public Map getExportedCustomDirectEventTypeConstants() {
        return MapBuilder.of(RNCalendarStripConstants.ON_DATE_CHANGE_TAG, MapBuilder.of("registrationName", RNCalendarStripConstants.ON_DATE_CHANGE_TAG));
    }

    /**
     * register specific events and pass data with callbacks
     * @param reactContext android context
     * @param rnCalendarStripView Recycler view of the RBCalendarStrip
     */
    @Override
    protected void addEventEmitters(final ThemedReactContext reactContext, final RNCalendarStripView rnCalendarStripView) {
        super.addEventEmitters(reactContext, rnCalendarStripView);
        rnCalendarStripView.getRecyclerView().addOnItemTouchListener(
                new RecyclerItemClickListener(reactContext, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (position != -1) {
                            rnCalendarStripView.emitChangedDate(reactContext, position);
                        }
                    }
                })
        );
    }
}

