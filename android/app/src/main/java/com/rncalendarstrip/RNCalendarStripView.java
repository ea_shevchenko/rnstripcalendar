package com.rncalendarstrip;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.uimanager.events.RCTEventEmitter;

import java.text.SimpleDateFormat;
import java.util.*;

import static com.rncalendarstrip.RNCalendarStripConstants.*;

public class RNCalendarStripView extends LinearLayout{

    private List<Date> data = new ArrayList<>();
    private RNCalendarStripRecyclerAdapter recyclerViewAdapter;
    private RNCalendarStripLayoutManager mLayoutManager;
    private RecyclerView recyclerView;

    private int listWidth, listHeight;
    private int dayNameSize, dayNumberSize;
    private int margin;
    private int startRange, endRange;
    private int rowWidth;
    private int rowThreshold;
    private int spaceWidth;
    private Date currentDate;

    private String dateFormat;
    private String datePropertyName;


    public RNCalendarStripView(Context context) {
        super(context);
        this.addView(initRootLinearLayout());
    }

    /**
     * setup root RNCalendarStrip layout
     */
    private LinearLayout initRootLinearLayout() {
        LinearLayout linearLayout = new LinearLayout(this.getContext());
        linearLayout.setOrientation(VERTICAL);
        initRNCalendarStripPropsParams();
        initCalendarDataWithRange(this.getStartRange(), this.getEndRange());

        initRNCalendarViewList(linearLayout);
        linearLayout.setMinimumHeight(this.getListHeight());
        linearLayout.setMinimumWidth(this.getListWidth());
        return linearLayout;
    }

    /**
     * setup properties of the RNCalendarStrip (default)
     */
    private void initRNCalendarStripPropsParams() {
        setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        this.setListHeight(DEFAULT_LIST_HEIGHT);
        this.setListWidth(DEFAULT_LIST_WIDTH);
        this.setDayNameSize(DEFAULT_DAY_NAMES_SIZE);
        this.setDayNameSize(DEFAULT_DAY_NAMES_SIZE);
        this.setDayNumberSize(DEFAULT_DAY_NUMBER_SIZE);
        this.setMargin(DEFAULT_MARGIN);
        this.setRowWidth(DEFAULT_ROW_WIDTH);
        this.setRowThreshold(DEFAULT_ROW_THRESHOLD);
        this.setSpaceWidth(DEFAULT_SPACE_WIDTH);
        this.setDateFormat(DEFAULT_DATE_FORMAT);
        this.setDatePropertyName(DEFAULT_DATE_PROPERTY_NAME);
        this.setStartRange(10);
        this.setEndRange(10);
    }

    /**
     * setup dates in range to populate RNCalendarStrip list
     * @param start start date
     * @param end   end date
     */
    private void initCalendarDataWithRange(int start, int end) {
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DATE, -start);
        Date dateStartBefore = calendar.getTime();
        currentDate = new Date();
        calendar.setTime(currentDate);
        calendar.add(Calendar.DATE, end);
        Date dateEndAfter = calendar.getTime();

        for (Date date = dateStartBefore; !date.equals(dateEndAfter); ) {
            calendar.setTime(date);
            calendar.add(Calendar.DATE, 1);
            date = calendar.getTime();
            data.add(date);
        }
    }

    /**
     * setup RNCalendarStrip list view
     * @param linearLayout root RN calendar list layout
     */
    private void initRNCalendarViewList(LinearLayout linearLayout) {
        recyclerView = new RecyclerView(getContext());
        mLayoutManager = new RNCalendarStripLayoutManager(getContext(), LinearLayout.HORIZONTAL, false);
        recyclerViewAdapter = new RNCalendarStripRecyclerAdapter(this, this.getRowWidth());

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(recyclerViewAdapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        RecyclerView.ItemDecoration itemDecoration = new SpacesItemDecoration(this.getSpaceWidth());
        recyclerView.addItemDecoration(itemDecoration);

        linearLayout.addView(recyclerView);
        registerRecyclerViewScrollEvent(recyclerView);

        int currentDatePosition = positionOfDate(currentDate);
        recyclerView.scrollToPosition(currentDatePosition);
    }

    /**
     * register on click and on scroll events in RNCalendarStrip recycler list
     * @param recyclerView
     */
    private void registerRecyclerViewScrollEvent(RecyclerView recyclerView) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemCount = mLayoutManager.findFirstVisibleItemPosition();
                int lastItem = firstVisibleItemCount + visibleItemCount;
                addNewDateOnScroll(firstVisibleItemCount, lastItem, totalItemCount);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                adjustPosition(recyclerView, newState);
            }
        });
    }

    /**
     * adjust position of RecyclerView
     * @param recyclerView RecyclerView
     * @param newState state of RecyclerView after scroll
     */
    private void adjustPosition(RecyclerView recyclerView, int newState) {
        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
            int i = 0;
            View child = recyclerView.getChildAt(i);
            while (child != null && child.getRight() <= 0) {
                child = recyclerView.getChildAt(++i);
            }
            if (child == null) {
                return;
            }
            final int left = child.getLeft();
            final int right = child.getRight();
            final int midpoint = recyclerView.getWidth() / 2;
            int LIST_LEFT_OFFSET = 5;
            if (left < LIST_LEFT_OFFSET) {
                if (right > midpoint) {
                    recyclerView.smoothScrollBy(left, 0);
                } else {
                    recyclerView.smoothScrollBy(right, 0);
                }
            }
        }
    }

    /**
     * emit string date in specific format
     * @param context Android context
     * @param position position of child view RNCalendarStrip
     */
    public void emitChangedDate(final ReactContext context, int position){
        Date date = this.getData().get(position);
        String dateStr = DateFormat.format(getDateFormat(), date).toString();

        WritableMap writableMap = new WritableNativeMap();
        writableMap.putString(getDatePropertyName(), dateStr);
        context.getJSModule(RCTEventEmitter.class).receiveEvent(getId(), RNCalendarStripConstants.ON_DATE_CHANGE_TAG, writableMap);
    }

    /**
     * add new date of the end of scroll view. first: calculated date of view, after check for condition
     * is item is last - we added new Date (timeToAdd variable)
     *
     * @param firstItem first visible item of the date list
     * @param lastItem last visible item of the date list
     * @param totalItemCount total items of the date list
     */
    private void addNewDateOnScroll(int firstItem, int lastItem, int totalItemCount) {
        if (notNullableData()) {
            long timeToAdd = 24 * 60 * 60 * 1000;
            final Date lastListDate = data.get(data.size() - 1);
            final Date firstListDate = data.get(0);
            final Date newDateAsc = new Date(lastListDate.getTime() + timeToAdd);
            final Date newDateDesc = new Date(firstListDate.getTime() - timeToAdd);
            ascScrollData(lastItem, totalItemCount, newDateAsc);
            descScrollData(firstItem, newDateDesc);
        }
    }

    /**
     * add new date into tail of dates list in up scrolling phase when last visible list item consist
     * @param lastItem last visible item of the date list
     * @param totalItemCount total items of the date list
     * @param newDateAsc  incremental date
     */
    private void ascScrollData(int lastItem, int totalItemCount, Date newDateAsc) {
        if (lastItem == totalItemCount) {
            data.add(newDateAsc);
            this.getRecyclerViewAdapter().notifyDataSetChanged();
        }
    }

    /**
     * add new date into head of dates list when first visible item equals first data item
     * @param firstItem first visible item of the date list
     * @param newDateDesc decremental date
     */
    private void descScrollData(int firstItem, Date newDateDesc){
        if(firstItem == 0){
            data.add(0, newDateDesc);
            this.getRecyclerViewAdapter().notifyDataSetChanged();
        }
    }

    /**
     * swap date list elements. First of all added decremental date and added all other exist items of the list
     * @param existDateList exists items of te list
     * @param dateDesc new decremental date
     * @return
     */
    private List<Date> swapDateList(List<Date> existDateList, Date dateDesc) {
        final List<Date> tmpList = new ArrayList<>();
        tmpList.add(dateDesc);
        for (int i = 0; i < existDateList.size(); i++) {
            tmpList.add(existDateList.get(i));
        }
        return tmpList;
    }

    /**
     * return position of date in listView ; Return 0 if date not exist.
     * Use row threshold parameter to resolve threshold position
     * @param date input date argument
     * @return
     */
    private int positionOfDate(Date date) {
        int position = 0;
        List<Date> currentDateList = this.getData();
        if (currentDateList != null && !currentDateList.isEmpty()) {
            for (int i = 0; i < currentDateList.size(); i++) {
                if (isDatesDaysEquals(date, currentDateList.get(i))) {
                    position = i - this.getRowThreshold();
                }
            }
        }
        return position;
    }

    /**
     * Return true if dates are equals
     * @param date1 first date argument to equal
     * @param date2 second date argument to equal
     * @return result of logical condition
     */
    private boolean isDatesDaysEquals(Date date1, Date date2) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(getDateFormat());
        if (dateFormat.format(date1).equals(dateFormat.format(date2))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * check for non nullable list data
     * @return result of logical condition
     */
    public boolean notNullableData(){
        return data != null && !data.isEmpty();
    }

    /**
     * getters and setters section
     * */
    public List<Date> getData() {
        return data;
    }

    public void setData(List<Date> data) {
        this.data = data;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    public RNCalendarStripRecyclerAdapter getRecyclerViewAdapter() {
        return recyclerViewAdapter;
    }

    public void setRecyclerViewAdapter(RNCalendarStripRecyclerAdapter recyclerViewAdapter) {
        this.recyclerViewAdapter = recyclerViewAdapter;
    }

    public RNCalendarStripLayoutManager getmLayoutManager() {
        return mLayoutManager;
    }

    public void setmLayoutManager(RNCalendarStripLayoutManager mLayoutManager) {
        this.mLayoutManager = mLayoutManager;
    }

    public int getListWidth() {
        return listWidth;
    }

    public void setListWidth(int listWidth) {
        this.listWidth = listWidth;
    }

    public int getListHeight() {
        return listHeight;
    }

    public void setListHeight(int listHeight) {
        this.listHeight = listHeight;
    }

    public int getDayNameSize() {
        return dayNameSize;
    }

    public void setDayNameSize(int dayNameSize) {
        this.dayNameSize = dayNameSize;
    }

    public int getDayNumberSize() {
        return dayNumberSize;
    }

    public void setDayNumberSize(int dayNumberSize) {
        this.dayNumberSize = dayNumberSize;
    }

    public int getRowWidth() {
        return rowWidth;
    }

    public void setRowWidth(int rowWidth) {
        this.rowWidth = rowWidth;
    }

    public int getRowThreshold() {
        return rowThreshold;
    }

    public void setRowThreshold(int rowThreshold) {
        this.rowThreshold = rowThreshold;
    }

    public int getSpaceWidth() {
        return spaceWidth;
    }

    public void setSpaceWidth(int spaceWidth) {
        this.spaceWidth = spaceWidth;
    }

    public int getMargin() {
        return margin;
    }

    public void setMargin(int margin) {
        this.margin = margin;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    public String getDatePropertyName() {
        return datePropertyName;
    }

    public void setDatePropertyName(String datePropertyName) {
        this.datePropertyName = datePropertyName;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public int getStartRange() {
        return startRange;
    }

    public void setStartRange(int startRange) {
        this.startRange = startRange;
    }

    public int getEndRange() {
        return endRange;
    }

    public void setEndRange(int endRange) {
        this.endRange = endRange;
    }
}
