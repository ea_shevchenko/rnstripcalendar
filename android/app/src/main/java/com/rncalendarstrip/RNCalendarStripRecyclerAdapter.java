package com.rncalendarstrip;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Date;

public class RNCalendarStripRecyclerAdapter extends RecyclerView.Adapter<RNCalendarStripRecyclerAdapter.RNCalendarStripHolder> {

    private RNCalendarStripView rnCalendarStripView;

    private final String formatDayName = "EEE";
    private final String formatDayNumber = "dd";
    private int rowWidth;

    public RNCalendarStripRecyclerAdapter(RNCalendarStripView rnCalendarStripView, int rowWidth) {
        this.rnCalendarStripView = rnCalendarStripView;
        this.rowWidth = rowWidth;
    }

    public class RNCalendarStripHolder extends RecyclerView.ViewHolder {

        private FrameLayout frameLayout;
        private LinearLayout linearLayout;
        private TextView dayNumber;
        private TextView dayName;

        public RNCalendarStripHolder(final View itemView) {
            super(itemView);
            frameLayout = (FrameLayout) itemView;
        }
    }

    /**
     * setup layouts and view elements into the RecyclerView
     * @param parent
     * @param viewType
     * @return view holder object. Released from view holder pattern
     */
    @Override
    public RNCalendarStripHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = new FrameLayout(parent.getContext());
        view.setLayoutParams(new RecyclerView.LayoutParams(rowWidth, RecyclerView.LayoutParams.MATCH_PARENT));

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        RNCalendarStripHolder viewHolder = new RNCalendarStripHolder(view);

        viewHolder.linearLayout = new LinearLayout(viewHolder.frameLayout.getContext());
        viewHolder.linearLayout.setOrientation(LinearLayout.VERTICAL);
        viewHolder.linearLayout.setBackgroundColor(Color.WHITE);
        viewHolder.linearLayout.setOrientation(LinearLayout.VERTICAL);
        viewHolder.linearLayout.setGravity(Gravity.CENTER);
        viewHolder.linearLayout.setLayoutParams(params);

        viewHolder.dayName = new TextView(viewHolder.frameLayout.getContext());
        viewHolder.dayName.setTextSize(rnCalendarStripView.getDayNameSize());
        viewHolder.dayName.setTextColor(Color.BLACK);
        viewHolder.dayName.setGravity(Gravity.CENTER);

        viewHolder.dayNumber = new TextView(viewHolder.frameLayout.getContext());
        viewHolder.dayNumber.setTextSize(rnCalendarStripView.getDayNumberSize());
        viewHolder.dayNumber.setTextColor(Color.BLACK);
        viewHolder.dayNumber.setGravity(Gravity.CENTER);

        viewHolder.frameLayout.addView(viewHolder.linearLayout);
        viewHolder.linearLayout.addView(viewHolder.dayName);
        viewHolder.linearLayout.addView(viewHolder.dayNumber);
        return viewHolder;
    }

    /**
     * bind values into view holder text elements
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(RNCalendarStripHolder holder, int position) {
        final Date day = rnCalendarStripView.getData().get(position);
        holder.dayNumber.setText(DateFormat.format(formatDayNumber, day).toString());
        holder.dayName.setText(DateFormat.format(formatDayName, day).toString());
    }

    @Override
    public int getItemCount() {
        return rnCalendarStripView.getData().size();
    }
}
