package com.rncalendarstrip;

import android.content.Context;
import android.graphics.PointF;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;

public class RNCalendarStripLayoutManager extends LinearLayoutManager{

    private Context context;
    private int orientation;
    private boolean reverseLayout;

    private final float SCROLL_SPEED = 1f;

    public RNCalendarStripLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
        this.context = context;
        this.orientation = orientation;
        this.reverseLayout = reverseLayout;
    }

    /**
     * Customize smooth scroll with LinearSmoothScroller. Also customize position and speed of scrolling
     * @param recyclerView recyclerView of RNCalendarStrip
     * @param state
     * @param position
     */
    @Override
    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
        super.smoothScrollToPosition(recyclerView, state, position);
        final LinearSmoothScroller smoothScroller = new LinearSmoothScroller(this.getContext()) {

            @Override
            public PointF computeScrollVectorForPosition(int targetPosition) {
                return this.computeScrollVectorForPosition(targetPosition);
            }

            @Override
            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return SCROLL_SPEED / displayMetrics.densityDpi;

            }
        };
        smoothScroller.setTargetPosition(position);
        startSmoothScroll(smoothScroller);
    }

    public Context getContext() {
        return context;
    }

    @Override
    public int getOrientation() {
        return orientation;
    }

    @Override
    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    public boolean isReverseLayout() {
        return reverseLayout;
    }

    @Override
    public void setReverseLayout(boolean reverseLayout) {
        this.reverseLayout = reverseLayout;
    }
}
