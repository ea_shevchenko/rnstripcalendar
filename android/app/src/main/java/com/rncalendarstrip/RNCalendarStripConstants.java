package com.rncalendarstrip;

public class RNCalendarStripConstants {

    private RNCalendarStripConstants() {
    }

    static final String RN_CALENDAR_STRIP_TAG = "CalendarStrip";
    static final String ON_DATE_CHANGE_TAG = "onDateChange";

    static final String DAY_NAME = "dayNameSize";
    static final String DAY_NUMBER = "dayNumberSize";
    static final String HEIGHT = "height";
    static final String WIDTH = "width";
    static final String MARGIN = "stripMargin";
    static final String ROW_WIDTH = "rowWidth";
    static final String DATE_FORMAT = "dateFormat";
    static final String DATE_PROPERTY_NAME = "dateProperty";

    static final int DEFAULT_SPACE_WIDTH  = 4;
    static final int DEFAULT_ROW_WIDTH = 200;
    static final int DEFAULT_LIST_HEIGHT = 100;
    static final int DEFAULT_LIST_WIDTH = 100;
    static final int DEFAULT_DAY_NAMES_SIZE = 12;
    static final int DEFAULT_DAY_NUMBER_SIZE = 17;
    static final int DEFAULT_MARGIN = 10;
    static final int DEFAULT_ROW_THRESHOLD = 2;
    static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
    static final String DEFAULT_DATE_PROPERTY_NAME = "date";
}
