/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ToastAndroid
} from 'react-native';

import CalendarStrip from './RNCalendarStrip';

export default class RNCalendarStrip extends Component {

    constructor() {
        super();
        this.state = {date: ''};
    }

    render() {
        let dateMessage = this.state.date;
        return (
            <View style={styles.container}>
                <CalendarStrip style={{width: 200, height: 200, backgroundColor: '#eff4f7'}}
                               height={50}
                               width={300}
                               dayNameSize={12}
                               dayNumberSize={25}
                               stripMargin={5}
                               startRange={2}
                               endRange={2}
                               rowWidth={200}
                               dateFormat="dd/MM"
                               dateProperty="date"
                               onDateChange={(event) => {
                                   const date = event.nativeEvent.date;
                                   this.setState({date: date});
                                   ToastAndroid.show(date, ToastAndroid.SHORT);
                               }}/>
                <Text style={styles.welcome}>
                    Welcome to React Native!
                </Text>
                <Text style={styles.instructions}>
                    Selected date: {'\n'}
                    {dateMessage}
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

AppRegistry.registerComponent('RNCalendarStrip', () => RNCalendarStrip);
