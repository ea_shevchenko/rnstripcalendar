import {PropTypes} from 'react';
import {requireNativeComponent, View} from 'react-native';

/**
 * React native strip calendar (with Recycler View native Android component implementation)
 * PROPS:
  * dayNameSize (default = 12) - size of the day name (used Android TextView) in RNCalendarStrip
  * dayNumberSize (default = 17) - size of the day number (used Android TextView) in RNCalendarStrip
  * height (default = 100) - height of the  RNCalendarStrip
  * width (default = 100) - width of the  RNCalendarStrip
  * stripMargin (default = 10) - margin between RNCalendarStrip rows (not tested!)
  * rowWidth (default = 200) - width of the RNCalendarStrip rows (not tested!)
  * dateFormat (default equals 'dd/MM/yyyy') - format of the date RNCalendarStrip
  * dateProperty (default equals 'date') - name of the key date object which returned from callback
  * onDateChange - called when user select date of the RNCalendarStrip.For example:
 * @code
 *  onDateChange={(event) => {
    const date = event.nativeEvent.date;
    this.setState({date: date});
    ToastAndroid.show(date, ToastAndroid.SHORT);
    }
 */

var iface = {
    name: 'RNCalendarStrip',
    propTypes: {
        dayNameSize: PropTypes.number,
        dayNumberSize: PropTypes.number,
        height: PropTypes.number,
        width: PropTypes.number,
        stripMargin: PropTypes.number,
        rowWidth: PropTypes.number,
        dateFormat: PropTypes.string,
        dateProperty: PropTypes.string,
        onDateChange:PropTypes.func,
        ...View.propTypes
    },
};

module.exports = requireNativeComponent('CalendarStrip', iface);